﻿using UnityEngine;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnityPathPointDebuger : MonoBehaviour
    {
        public Vector3[] pathArr;


        void OnDrawGizmos()
        {
            if (pathArr == null) return;

            var oldColor = Gizmos.color;
            Gizmos.color = Color.green;

            for (int i = 1; i < pathArr.Length; i++)
            {
                if (i == pathArr.Length - 1) Gizmos.color = Color.red;

                var x = pathArr[i - 1];
                var y = pathArr[i];

                Gizmos.DrawSphere(x, 3f);
                Gizmos.DrawSphere(y, 3f);
                Gizmos.DrawLine(x, y);
            }
            Gizmos.color = oldColor;
        }
    }
}
