﻿using UnityEngine;
using System.Collections;

namespace Hont
{
    public class HontOCTTree<T>
    {
        OCTNode<T> mRoot;

        public OCTNode<T> Root { get { return mRoot; } }


        public HontOCTTree(Bounds size, int treeDepth)
            : this(size, treeDepth, new OCTParentNodeEmptySyncer<T>())
        {
        }

        public HontOCTTree(Bounds size, int treeDepth, IOCTNodeSyncer<T> syncer)
        {
            mRoot = new OCTNode<T>(size.min, size.max, syncer);
            mRoot.Subdivide(treeDepth);
        }
    }
}
